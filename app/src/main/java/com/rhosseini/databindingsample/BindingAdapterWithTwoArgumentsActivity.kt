package com.rhosseini.databindingsample

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.rhosseini.databindingsample.databinding.ActivityBindingAdapterWithTwoArgumentsBinding

class BindingAdapterWithTwoArgumentsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityBindingAdapterWithTwoArgumentsBinding =
            DataBindingUtil.setContentView(
                this,
                R.layout.activity_binding_adapter_with_two_arguments
            )
    }
}
