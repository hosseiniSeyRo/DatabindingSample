package com.rhosseini.databindingsample

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.rhosseini.databindingsample.databinding.ActivityIntentHandlerBinding

class IntentHandlerActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityIntentHandlerBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_intent_handler)

        binding.handler = this

        // this approach is work too
//        binding.floatingActionButton.setOnClickListener(View.OnClickListener {
//            val intent = Intent(this, MainActivity::class.java)
//            startActivity(intent)
//        })
    }

    fun goToNextPage(view: View) {
        val intent = Intent(view.context, MainActivity::class.java)
        view.context.startActivity(intent)
    }
}
