package com.rhosseini.databindingsample

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.rhosseini.databindingsample.databinding.ActivityLiveDataBinding

class LiveDataActivity : BaseActivity() {

    // Obtain ViewModel from ViewModelProviders
    private val viewModel by lazy { ViewModelProvider(this).get(SimpleViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding:ActivityLiveDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_live_data)

        binding.lifecycleOwner = this  // use Fragment.viewLifecycleOwner for fragments

        binding.viewModel = viewModel
    }
}
