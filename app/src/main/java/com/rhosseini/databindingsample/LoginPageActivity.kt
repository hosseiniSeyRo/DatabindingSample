package com.rhosseini.databindingsample

import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.rhosseini.databindingsample.data.LoginViewModel
import com.rhosseini.databindingsample.databinding.ActivityLoginPageBinding

class LoginPageActivity : BaseActivity() {

    // Obtain ViewModel from ViewModelProviders
    private val viewModel by lazy { ViewModelProvider(this).get(LoginViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityLoginPageBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_login_page)
        binding.lifecycleOwner = this


        binding.loginViewModel = viewModel

        binding.login.setOnClickListener {
            if (viewModel.validate()) {
                Snackbar.make(
                    binding.getRoot(),
                    viewModel.email?.get() + " — " + viewModel.password?.get(),
                    Snackbar.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(this, "login failed", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
