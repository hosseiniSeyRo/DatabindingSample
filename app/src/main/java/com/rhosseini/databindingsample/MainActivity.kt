package com.rhosseini.databindingsample

import android.content.Intent
import android.os.Bundle
import android.widget.Button

class MainActivity : BaseActivity() {

    private lateinit var btnSimpleBinding: Button
    private lateinit var btnTextWatcher: Button
    private lateinit var btnTextWatcherAndObservableField: Button
    private lateinit var btnLiveData: Button
    private lateinit var btnIntentHandler: Button
    private lateinit var btnLoginPage: Button
    private lateinit var btnRecyclerView: Button
    private lateinit var btnBindingAdapterWithTwoArguments: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bindViews()

        clickHandler()
    }

    private fun bindViews() {
        btnSimpleBinding = findViewById(R.id.btnSimpleBinding)
        btnTextWatcher = findViewById(R.id.btnTextWatcher)
        btnTextWatcherAndObservableField =
            findViewById(R.id.btnTextWatcherAndObservableField)
        btnLiveData = findViewById(R.id.btnLivaData)
        btnIntentHandler = findViewById(R.id.btnIntentHandler)
        btnLoginPage = findViewById(R.id.btnLoginPage)
        btnRecyclerView = findViewById(R.id.btnRecyclerView)
        btnBindingAdapterWithTwoArguments = findViewById(R.id.btnBindingAdapterWithTwoArguments)
    }

    private fun clickHandler() {
        btnSimpleBinding.setOnClickListener {
            startActivity(Intent(this, SimpleBindingActivity::class.java))
        }

        btnTextWatcher.setOnClickListener {
            startActivity(Intent(this, TextWatcherActivity::class.java))
        }

        btnTextWatcherAndObservableField.setOnClickListener {
            startActivity(Intent(this, TextWatcherAndObservableFieldActivity::class.java))
        }

        btnLiveData.setOnClickListener {
            startActivity(Intent(this, LiveDataActivity::class.java))
        }

        btnIntentHandler.setOnClickListener {
            startActivity(Intent(this, IntentHandlerActivity::class.java))
        }

        btnLoginPage.setOnClickListener {
            startActivity(Intent(this, LoginPageActivity::class.java))
        }

        btnRecyclerView.setOnClickListener {
            startActivity(Intent(this, RecyclerViewActivity::class.java))
        }

        btnBindingAdapterWithTwoArguments.setOnClickListener {
            startActivity(Intent(this, BindingAdapterWithTwoArgumentsActivity::class.java))
        }
    }
}
