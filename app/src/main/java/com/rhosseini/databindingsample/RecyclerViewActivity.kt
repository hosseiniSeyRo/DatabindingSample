package com.rhosseini.databindingsample

import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.rhosseini.databindingsample.data.User
import com.rhosseini.databindingsample.databinding.ActivityRecyclerViewBinding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class RecyclerViewActivity : BaseActivity() {

    private lateinit var binding: ActivityRecyclerViewBinding
    private val userList = ArrayList<User>()
    private lateinit var mAdapter: UserAdapter
    private lateinit var recyclerView: RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_recycler_view)
        binding.lifecycleOwner = this

        setDummyData()

        initRecyclerView()
    }

    private fun setDummyData() {
        userList.addAll(
            listOf(
                User("tehran", "021", 28, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS4sqKv9ucq2GeXqlGQcMBfnfOzo4qr8gXv_m4dihgkMnHkfYw8pw"),
                User("ali ahmadi", "sdf@dfg.rftg", 5, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTSPRkRvJw8HBB5aPwdYGkSb34wivwN3L0ATKCrYXQTE1KL0iuGVg"),
                User("rastin samsami", "tyjh@uik.dr", 8, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS4sqKv9ucq2GeXqlGQcMBfnfOzo4qr8gXv_m4dihgkMnHkfYw8pw"),
                User("samad hosseini", "ty@op.df", 25, "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Woman_5.jpg/300px-Woman_5.jpg"),
                User("pooyan hosseini", "trk@cfgj.thb", 33, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTSPRkRvJw8HBB5aPwdYGkSb34wivwN3L0ATKCrYXQTE1KL0iuGVg"),
                User("yalda soleimani", "prthyojrthj@dfgd.dfg", 54, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS4sqKv9ucq2GeXqlGQcMBfnfOzo4qr8gXv_m4dihgkMnHkfYw8pw")
            )
        )
    }

    private fun initRecyclerView() {
        recyclerView = binding.rv
        mAdapter = UserAdapter(object : BaseAdapter.OnItemClickListener<User> {
            override fun onItemClick(item: User) {
                Toast.makeText(this@RecyclerViewActivity, ""+item.name, Toast.LENGTH_SHORT).show()
            }
        })
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.adapter = mAdapter

        mAdapter.addData(userList)
    }
}
