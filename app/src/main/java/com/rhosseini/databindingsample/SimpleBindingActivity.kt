package com.rhosseini.databindingsample

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.rhosseini.databindingsample.databinding.ActivitySimpleBindingBinding

class SimpleBindingActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivitySimpleBindingBinding= DataBindingUtil.setContentView(this, R.layout.activity_simple_binding)

        val firstName = "rastin"
        val lastName = "samsami"

        binding.firstName = firstName
        binding.lastName = lastName
    }
}
