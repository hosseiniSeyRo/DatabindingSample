package com.rhosseini.databindingsample

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.rhosseini.databindingsample.databinding.ActivityTextWatcherBinding

class TextWatcherActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityTextWatcherBinding = DataBindingUtil.setContentView(this, R.layout.activity_text_watcher)

    }
}
