package com.rhosseini.databindingsample

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.rhosseini.databindingsample.data.TextWatcherViewModel
import com.rhosseini.databindingsample.databinding.ActivityTextWatcherAndObservableFieldBinding

class TextWatcherAndObservableFieldActivity : BaseActivity() {

    // Obtain ViewModel from ViewModelProviders
    private val viewModel by lazy { ViewModelProvider(this).get(TextWatcherViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityTextWatcherAndObservableFieldBinding =
            DataBindingUtil.setContentView(
                this,
                R.layout.activity_text_watcher_and_observable_field
            )

        binding.viewModel = viewModel
    }
}
