package com.rhosseini.databindingsample

import com.rhosseini.databindingsample.data.User


class UserAdapter(itemClickListener: OnItemClickListener<User>) :
    BaseAdapter<User>(itemClickListener) {

    private var data: ArrayList<User> = ArrayList()

    override fun getItemCount(): Int = data.size

    override fun getItemForPosition(position: Int): User = data[position]

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.row_user_item
    }

    fun addData(data: ArrayList<User>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun updateData(data: ArrayList<User>) {
        this.data = data
        notifyDataSetChanged()
    }
}