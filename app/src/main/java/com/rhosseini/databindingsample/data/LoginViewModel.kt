package com.rhosseini.databindingsample.data

import androidx.databinding.ObservableField
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import androidx.lifecycle.ViewModel


class LoginViewModel : ViewModel() {

    var email = ObservableField<String>()
    var password = ObservableField<String>()
    var emailError = ObservableField<String>()
    var passwordError = ObservableField<String>()
    var existingUser = ObservableField<Boolean>(false)


    fun reset() {
        email.set(null)
        password.set(null)
        emailError.set(null)
        passwordError.set(null)
        existingUser.set(false)
    }

    fun validate(): Boolean {
        var valid: Boolean = true
        emailError.set(null)
        passwordError.set(null)

        if (email.get() == null || email.get().toString().isEmpty()) {
            emailError.set("mandatory field")
            valid = false
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(email.get().toString()).matches()) {
                emailError.set("invalid email")
                valid = false
            }
        }

        if (existingUser.get() == true && (password.get() == null || password.get().toString().isEmpty())) {
            passwordError.set("mandatory field")
            valid = false
        }

        return valid
    }

    fun setExistingUser(exist: Boolean) {
        existingUser.set(exist)
    }
}
