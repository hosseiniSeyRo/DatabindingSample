package com.rhosseini.databindingsample.data

data class User(
    val name: String,
    val email: String,
    val age : Int,
    val imageUrl: String
)