package com.rhosseini.databindingsample.util

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.databinding.BindingAdapter
import com.rhosseini.databindingsample.Popularity
import com.rhosseini.databindingsample.R
import com.squareup.picasso.Picasso
import com.bumptech.glide.Glide


@BindingAdapter("bind:letterNumberForTextWatcher")
fun onTextChange(et: EditText, number: Int) {
    et.addTextChangedListener(object : TextWatcher {
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if (s.length >= number) {
                et.setBackgroundColor(Color.GREEN)
            } else {
                et.setBackgroundColor(Color.YELLOW)
            }
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun afterTextChanged(s: Editable) {}
    })
}

@BindingAdapter("bind:setErrorBackground")
fun setErrorBackground(view: View, error: Boolean) {
    if (error) {
        view.setBackgroundColor(Color.BLUE)
    } else {
        view.setBackgroundColor(Color.RED)
    }
}

@BindingAdapter(value = ["bind:errorView", "bind:minimumCharacters"], requireAll = true)
fun textWatcher(et: EditText, errorView: View, min: Int) {
    et.addTextChangedListener(object : TextWatcher {
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            errorView.visibility = if (s.length >= min) View.GONE else View.VISIBLE
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun afterTextChanged(s: Editable) {}
    })
}

/**
 * A Binding Adapter that is called whenever the value of the attribute `bind:popularityIcon`
 * changes. Receives a popularity level that determines the icon and tint color to use.
 */
@BindingAdapter("bind:popularityIcon")
fun popularityIcon(view: ImageView, popularity: Popularity) {

    val color = getAssociatedColor(popularity, view.context)

    ImageViewCompat.setImageTintList(view, ColorStateList.valueOf(color))

    view.setImageDrawable(getDrawablePopularity(popularity, view.context))
}

/**
 * A Binding Adapter that is called whenever the value of the attribute `bind:progressTint`
 * changes. Depending on the value it determines the color of the progress bar.
 */
@BindingAdapter("bind:progressTint")
fun tintPopularity(view: ProgressBar, popularity: Popularity) {

    val color = getAssociatedColor(popularity, view.context)

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        view.progressTintList = ColorStateList.valueOf(color)
    }
}

/**
 *  Sets the value of the progress bar so that 5 likes will fill it up.
 *
 *  Showcases Binding Adapters with multiple attributes. Note that this adapter is called
 *  whenever any of the attribute changes.
 */
@BindingAdapter(value = ["bind:progressScaled", "bind:max"], requireAll = true)
fun setProgress(progressBar: ProgressBar, likes: Int, max: Int) {
    progressBar.progress = (likes * max / 5).coerceAtMost(max)
}

/**
 * Unused Binding Adapter to replace the Binding Converter that hides a view if the number
 * of likes is zero.
 */
@BindingAdapter("bind:visibleGone")
fun showHide(view: View, number: Int) {
    view.visibility = if (number == 0) View.GONE else View.VISIBLE
}

private fun getAssociatedColor(popularity: Popularity, context: Context): Int {
    return when (popularity) {
        Popularity.NORMAL -> context.theme.obtainStyledAttributes(
            intArrayOf(android.R.attr.colorForeground)
        ).getColor(0, 0x000000)
        Popularity.POPULAR -> ContextCompat.getColor(context, R.color.popular)
        Popularity.STAR -> ContextCompat.getColor(context, R.color.star)
    }
}

private fun getDrawablePopularity(popularity: Popularity, context: Context): Drawable? {
    return when (popularity) {
        Popularity.NORMAL -> {
            ContextCompat.getDrawable(context, R.drawable.ic_person_black_96dp)
        }
        Popularity.POPULAR -> {
            ContextCompat.getDrawable(context, R.drawable.ic_whatshot_black_96dp)
        }
        Popularity.STAR -> {
            ContextCompat.getDrawable(context, R.drawable.ic_whatshot_black_96dp)
        }
    }
}

@BindingAdapter(value = ["bind:imageUrl", "bind:placeHolder"], requireAll = false)
fun loadImageByUrl(view: ImageView, url: String, placeHolder: Int) {
//    Picasso.get().load(url).into(view)
    val requestCreator = Picasso.get().load(url)
    if (placeHolder != 0) {
        requestCreator.placeholder(placeHolder)
    }
    requestCreator.into(view)

    //by glide
//    Glide
//        .with(view.context)
//        .load(url)
//        .centerCrop()
//        .placeholder(placeHolder)
//        .into(view)
}


